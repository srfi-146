(import scheme)
(import (chicken base))
(import (srfi 1))
(import (srfi 128))
(import (srfi 146))
(import (srfi 146 hash))
(import test)

(define-syntax test-equal
  (syntax-rules ()
    ((_ expected expression)
     (test expected expression))
    ((_ name expected expression)
     (test name expected expression))))

(include "srfi-146.scm")
(include "srfi-146-hash.scm")
(test-exit)
