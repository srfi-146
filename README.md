## About

Port of the
[SRFI-146](https://srfi.schemers.org/srfi-146/srfi-146.html)
(Mappings) reference implementation to CHICKEN.

## Docs

See [its wiki page].

[its wiki page]: https://wiki.call-cc.org/eggref/5/srfi-146
