(module (srfi 146 hash)
        (hashmap hashmap-unfold
         hashmap? hashmap-contains? hashmap-empty? hashmap-disjoint?
         hashmap-ref hashmap-ref/default hashmap-key-comparator
         hashmap-adjoin hashmap-adjoin!
         hashmap-set hashmap-set!
         hashmap-replace hashmap-replace!
         hashmap-delete hashmap-delete! hashmap-delete-all hashmap-delete-all!
         hashmap-intern hashmap-intern!
         hashmap-update hashmap-update! hashmap-update/default hashmap-update!/default
         hashmap-pop hashmap-pop!
         hashmap-search hashmap-search!
         hashmap-size hashmap-find hashmap-count hashmap-any? hashmap-every?
         hashmap-keys hashmap-values hashmap-entries
         hashmap-map hashmap-map->list hashmap-for-each hashmap-fold
         hashmap-filter hashmap-filter!
         hashmap-remove hashmap-remove!
         hashmap-partition hashmap-partition!
         hashmap-copy hashmap->alist alist->hashmap alist->hashmap!
         hashmap=? hashmap<? hashmap>? hashmap<=? hashmap>=?
         hashmap-union hashmap-intersection hashmap-difference hashmap-xor
         hashmap-union! hashmap-intersection! hashmap-difference! hashmap-xor!
         make-hashmap-comparator
         hashmap-comparator
         comparator?)
  (import scheme)
  (import (chicken base))
  (import (chicken platform))
  (import (srfi 1))
  (import (srfi 128))
  (import (srfi 145))
  (import (srfi 158))
  (import hash-trie)
  (register-feature! 'srfi-146-hash)

  (define phm/put hash-trie/insert)
  (define phm/remove hash-trie/delete)
  (define (phm/get phm key #!optional default)
    (hash-trie/lookup phm key default))
  (define (make-phm hash =)
    (make-hash-trie (make-hash-trie-type = hash)))
  (define (phm/for-each proc phm)
    (hash-trie/fold phm
                    (void)
                    (lambda (key value acc)
                      (proc key value)
                      acc)))

  (include "srfi-146/srfi/146/hash.scm")
)
